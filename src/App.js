
import Navbar from './components/Navbar';
import Login from './pages/Login';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Register from './pages/Register';
import Home from './pages/Home';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import AdminDb from './pages/AdminDb';
import CreateItem from './pages/CreateItem';
import Archived from './pages/Archived';
import EditProduct from './pages/EditProduct';
import DeleteItems from './pages/DeleteItems';
import ProductDetails from './pages/ProductDetails';
import Sidebar from './components/Sidebar';
import Footer from './components/Footer';
import MyOrders from './pages/MyOrders';
import Restore from './components/Restore';



function App() {
  
  const [user, setUser] = useState(null);
  const [userID, setUserID]= useState(null);
  
  
  const unSetUser = () => {
    localStorage.clear();
    
  }

  useEffect(() => {
      fetch("http://localhost:4005/user/", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {
        if(localStorage.getItem('token')!==null){
          setUser({
            id:data._id
          })
          setUserID(data.isAdmin)
        }else{
          setUser(null)
        }
      
      })
  }, [])

 
  return (
    <UserProvider value = {{user, setUser, unSetUser, userID}}>
        <Router>
      
        <Navbar />  
        <Routes>
        <Route path='/myorders/:_id' element={<MyOrders />} />
        <Route path='/product/:_id' element = {<ProductDetails />} />
        <Route path='/admin/restore/:_id' element ={<Restore />} />
        <Route path='/admin/delete/:_id' element ={<DeleteItems />} />
        <Route path='/admin/edit/:_id' element ={<EditProduct />} />
        <Route path='/admin/deleted' element ={<Archived />} />
        <Route path='/admin/addproduct' element ={<CreateItem />} />
        <Route path='/admin' element = {<AdminDb />} />
        <Route path='/*' element = {<PageNotFound />}/>
        <Route path='/'  element ={<Home />} />
       <Route path='/register' element={ <Register />} />
       <Route path='/login' element= { <Login />} />
       <Route path='/logout' element = {<Logout />} />
        </Routes>
        <Sidebar />
        <Footer />
        </Router>
        </UserProvider>
    
  );
}

export default App;
