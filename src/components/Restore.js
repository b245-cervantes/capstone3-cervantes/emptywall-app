import React, { useState, useEffect } from 'react'
import { json, Link, useParams } from 'react-router-dom';
import { Modal } from 'antd';
import { useNavigate } from 'react-router-dom';

function Restore() {

    const [isActive, setIsActive] = useState("");
    const {_id} = useParams();
    const navigate = useNavigate();

    useEffect(()=>{
        fetch(`http://localhost:4005/product/${_id}`)
  
  .then(result => result.json())
  .then(data => {
    setIsActive(data.isActive)
    

  })
},[_id])


const item = {isActive}


    const DeleteItemses =(event, id)=>{
        event.preventDefault();

        fetch(`http://localhost:4005/product/archived/${_id}`, {
            method: 'PUT',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`

            },
            body: JSON.stringify(item)
        })
        .then(result => json())
        .then(data => {
            console.log(data)
            Modal.success({
                content: 'The product has been successfully restored!!',
                okText: 'save',
                okButtonProps: 'ghost'
            })
            navigate('/admin');
        })
    }

  return (
    <div className='container mx-auto w-full 100v'>
    <div className='flex flex-col justify-center items-center mt-[20%]'>
            <h2 className='text-2xl font-poppins -mt-11 mb-5'>Restore product</h2>
        <form className='flex flex-col w-[20rem]' onSubmit={event => DeleteItemses(event)}>
            <input type='text' placeholder='isActive' value={isActive} 
                onChange={event => setIsActive(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none'/>
        
            <button className='p-3 bg-black text-white rounded-md mt-5'>DELETE ITEM</button>
            <button type='button' className='p-3 bg-black text-white rounded-md mt-5'><Link as = {Link} to = '/admin'>BACK</Link></button>
        </form>
        </div>
    </div>
  )
}

export default Restore