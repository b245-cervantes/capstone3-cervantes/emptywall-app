import React, { useContext }from 'react'
import { Link } from 'react-router-dom'
import { BsPlus, BsEyeFill } from 'react-icons/bs'

//import cart context
import { CartContext } from '../CartContext';


function Product({product}) {
     const { addToCart } = useContext(CartContext)
  
    const {_id, name, description, img, size, color, price, category} = product;
   return <div>
    <div className='lg:border lg:border-[#e4e5e4] h-[285px] sm:border-none  mb-4 relative overflow-hidden group transition'>
        <div className='w-full h-full flex justify-center items-center'>
            {/* img */}
            <div className='w-[310px] mx-auto flex justify-center items-center'>
                <img className='max-h-[390px] group-hover:scale-110 transition duration-300' src={img} alt=''/>
            </div>
            {/* button */}
            <div className='absolute top-0 -right-11 group-hover:right-4 lg:mt-5 p-2 flex flex-col items-center justify-center gap-y-2 opacity-0 group-hover:opacity-100 
            transition-all duration-300'>
                <button onClick={() => addToCart(product, _id)}>
                <div className='flex justify-center items-center text-white w-12 h-12 bg-gradient-to-r from-red-500 to-yellow-500'>
                    <BsPlus className='text-3xl' />
                </div>
                </button>
                <Link to={`/product/${_id}`} className='w-12 h-12 bg-white flex justify-center items-center text-primary drop-shadow-xl'>
                    <BsEyeFill />
                </Link>
            </div>
        </div>
    </div>
    {/* category & title $ price */}
    <div className='text-sm capitalize text-gray-500 mb-1'>{category}</div>
    <Link to = {`/product/${_id}`}>
    <h2 className='font-semibold mb-1'>{name}</h2>
    </Link>
    <div className='font-semibold'>₱ {price}</div>

   </div>

}

export default Product