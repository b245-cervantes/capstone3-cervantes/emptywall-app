import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { IoMdAdd, IoMdClose, IoMdRemove } from 'react-icons/io';
//import cart context;
import { CartContext } from '../CartContext';
import { useContext } from 'react';
import UserContext from '../UserContext';

import Modal from 'antd/es/modal/Modal';
import Swal from 'sweetalert2';


function CartItem({item}) {

    const { removeFromCart, increaseAmount, decreaseAmount, clearCart } = useContext(CartContext);
   




    const {_id,name,size,img,price,amount} = item;

    const {user} = useContext(UserContext);
    const [userId, setUserId] = useState({user});
    const [product, setProduct] = useState('');
    const [productId, setProductId] = useState(_id);
    const [quantity, setQuantity] = useState(amount);
    const [subTotal, setSubTotal] = useState(price * amount);

    const createOrder = () => {
    
        fetch('http://localhost:4005/order/', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({
                userId: userId,
                product:product,
                productId: productId,
                quantity: quantity,
                subTotal: subTotal
            })
        })
        .then(result => result.json())
        .then(data => {
            
            if(data) {
                Modal.success({
                    content: 'Checkout successfullt!!',
                    okText: 'save',
                    okButtonProps: 'ghost'
                })
            }
            clearCart()
        })
        
    }
    
  return (
      
    <div className='flex gap-x-4 py-2 lg:px-6 border-b border-gray-200 w-full font-light text-gray-500'>
        <div className='w-full min-h-[150px] flex items-center gap-x-4'>
            {/* img */}
            <Link to={`/product/${_id}`} >
                <img className='max-w-[80px]' src={img} alt='' />
            </Link>
            <div className='w-full flex flex-col'>
                {/* title $ remove icon */}
                <div className='flex justify-between items-center mb-2'>
                    <Link to={`/product/${_id}`} className='text-sm uppercase font-medium max-w[240px] text-primary hover:underline'>{name}</Link>
                    {/* remove */}
                    <div onClick={()=> removeFromCart(_id)} className='text-xl cursor-pointer'>
                       <IoMdClose className='text-gray-500 hover:text-red-500 transition'/> 
                    </div>
                </div>
                <div className='flex gap-x-2 h-[36px] text-sm'>
                {/* quantity */}
                    
                        <div className='flex flex-1 max-w-[100px] items-center h-full border text-primary font-medium'>
                            {/* minus icon */}
                            <div onClick ={() => decreaseAmount(_id)} className='flex-1 h-full flex justify-center items-center cursor-pointer'>
                            <IoMdRemove />
                            </div>
                            {/* amout */}
                            <div className='h-full flex justify-center items-center px-2'>{amount}</div>
                            <div onClick={() => increaseAmount(_id)} className='flex-1 h-full flex justify-center items-center cursor-pointer'>
                                <IoMdAdd />
                                {/* plus icon */}
                            </div>
                         
                        </div>
                        
                        {/* item price */}
                        <div className='flex-1 flex items-center justify-around text-primary font-medium'> ₱ {parseFloat(price).toFixed(2)}</div>
                        {/* final price */}
                        <div className='flex-1 flex justify-end items-center text-black font-bold'>{`₱ ${parseFloat(price * amount).toFixed(2)}`}</div>
                    
                </div>
            </div>
        </div>
        <div className='bottom-[10%] absolute flex justify-between items-center w-full'>
        <Link to='/' className='bg-gray-200 flex p-4 justify-center items-center mr-10 text-primary w-full font-medium'>View Cart</Link>
        <button onClick={user ? event => createOrder(event): Swal.fire({
            title: "ERROR",
            icon:"error"
        })}  className='bg-black text-white flex justify-center mr-[100px] items-center p-4 w-full font-medium'>Checkout</button>
        </div>
    </div>
    
  )
}

export default CartItem