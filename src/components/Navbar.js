import React, { useEffect } from 'react';
import Logo from '../assets/logo.png';
import { BiSearchAlt , BiHeart, BiShoppingBag } from 'react-icons/bi';
import SaleBanner from './SaleBanner';
import { Link, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext, Fragment } from 'react';
import { SidebarContext } from '../SidebarContext';
import { CartContext } from '../CartContext';
import { useState } from 'react';
import { Navigate } from 'react-router-dom';

function Navbar() {
    const [isOn,setIsOn] = useState(false);
   const {isOpen,setIsOpen} = useContext(SidebarContext)
   const {itemAmount} = useContext(CartContext)
  const { _id } = useParams();
  const {user, userID} = useContext(UserContext);
  //event listner

  useEffect(()=> {
    window.addEventListener('scroll', ()=> {
      window.scrollY > 60 ? setIsOn(true): setIsOn(false);
    })
  })

  return (
            
    <div className='xl:container xl:mx-auto'>
      {/* SALE BANNER ON TOP */}
      
      {
       
        <Fragment>
        <SaleBanner/>
        <div className='w-full h-40px mt-2 hidden md:inline-block'>
          <ul className='flex justify-end space-x-5 text-xs mr-10 font-poppins text-gray-500'>
            <li><Link as = {Link} to = '/'>About Us</Link></li>
            <li className='border-x-[1px] border-gray-400 px-3'><Link>Help</Link></li>
            { 
              user ?
              <Fragment>
              <li className= 'border-gray-400 border-r-[1px] pr-3 '><Link as = {Link} to = {`/myorders/${_id}`}>My orders</Link></li>
              <li className= 'border-gray-400 '><Link as = {Link} to = '/logout'>Logout</Link></li>
              
              </Fragment>
              :
              <Fragment>
                <li className=''><Link as = {Link} to = '/register'>Register</Link></li>
                <li className='border-l-[1px] border-gray-400 pl-3'><Link as = {Link} to = '/login'>Sign In?</Link></li>
             </Fragment>
            }
          
          </ul>
      </div>
      {/* LOGO */}
      
      <nav className={`${isOn ? 'bg-white-400 py-6 shadow-md':'bg-none py-6 top-0'} w-full h-5 mt-5 flex justify-between items-center z-10 transition-all`}>
        <div className='ml-10'>
          <img src={Logo} alt="Logo image" style={{width: '50px'}} />
        </div>
        {/* nav-links */}
        <div className='-mt-7 md:mr-[12%]'>
          <ul className='hidden lg:flex relative lg:absolute space-x-7'>
            <Link to='/'>Men</Link>
            <Link to='/'>Women</Link>
            <Link to='/'>Home</Link>
            <Link to='/'>Sale</Link>
            <Link to='/'>Caps</Link>
          </ul>
        </div>
        {/* SEARCH BARS AND ICONS */}
       <form className='flex absolute md:absolute lg:relative w-70 right-0 mr-[15%] md:mr-[12%] lg:mr-[-25%] md:-mt-5 lg:mt-1'>
       <input type='text' placeholder='Search' name='search' className='bg-gray-100 text-left p-1 pl-10 w-[80%] rounded-full
         hidden lg:flex outline-none hover:bg-gray-300'/>
        <button type='submit' className='md:absolute lg:mt-[2px] md:-mt-[10] sm:mr-[29%] sm:right-0 md:mr-[85%]
        hover:bg-gray-300 p-[2px] rounded-full'><BiSearchAlt size={26} /></button>
       </form>
       <div className='flex space-x-3 mr-10'>
        <button className='hidden lg:flex'><BiHeart size={26}/></button>
        <div onClick={() => setIsOpen(!isOpen)} className='cursor-pointer flex relative max-w-[50px]'><BiShoppingBag size={26}/>
            <div className='bg-red-500 absolute -right-2 -bottom-2 text-[12px] w-[18px] h-[18px] text-white rounded-full flex justify-center items-center'>{itemAmount}</div>
        </div>
       </div>
      </nav>
      
      
      
      </Fragment>
      }

        
    
      </div>
      
      
  )
  
}

export default Navbar

