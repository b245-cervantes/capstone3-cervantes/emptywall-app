import React from 'react'


import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import UserContext from '../UserContext';

import { Link } from 'react-router-dom';
import {  Modal } from 'antd';






function EditProd() {
  
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [img, setImg] = useState("");
    const [size, setSize] =useState("")
    const [color, setColor] = useState("");
    const [price, setPrice] = useState("");
    const [category, setCategory] = useState("");
    const {_id} = useParams();
    const {user, setUser} = useContext(UserContext);
      const navigate = useNavigate();

      useEffect(()=>{
            fetch(`http://localhost:4005/product/${_id}`)
      
      .then(result => result.json())
      .then(data => {
        console.log(data.name)
        setName(data.name)
        setDescription(data.description)
        setImg(data.img)
        setSize(data.size)
        setColor(data.color)
        setPrice(data.price)
        setCategory(data.category)
    
      })
    },[_id])



    const prods = {name, description, img, size, color, price, category}
      function editedProduct (event, id) {
        event.preventDefault();

        fetch(`http://localhost:4005/product/update/${_id}`, {
            method: 'PUT',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`

            },
            body: JSON.stringify(prods)
        })
        .then(result => result.json())
        .then(data => {
            console.log(data)
            Modal.success({
                content: 'The product has been successfully updated!',
                okText: 'save',
                okButtonProps: 'ghost'
            })
            navigate('/admin');

           
                    
             
         })
      }

  return (
    user === false ?
    <Navigate to = '/*' />
        :
    <div className='container mx-auto 100v w-full'>
    <div className='flex flex-col justify-center items-center w-full'>
    <h2 className='text-2xl font-poppins mt-[100px] mb-5'>Update Product</h2>
    
    <form className='flex flex-col justify-center items-center' onSubmit={(event)=>editedProduct(event,_id)} >
    <input type='text' placeholder='Product name' value={name}  onChange = {event => setName(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none w-[25rem]' required/>
      <textarea type='text' placeholder='Product Description' value={description}  onChange = {event => setDescription(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none w-[25rem]' required></textarea>
     <input type='text' placeholder='insert img link' value={img}  onChange = {event => setImg(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none w-[25rem]' required/>
    <div className='flex items-center justify-center'>
     <label htmlFor='size' className='mr-5'>Sizes :</label>
   
     <select name='size' id='size' className='border w-[21rem]' onChange={event => setSize(event.target.value)}>
        <option value='small'>Small</option>
        <option value='medium'>Medium</option>
        <option value='large'>Large</option>
        <option value='x-large'>Extra Large</option>
     </select>
     </div>

     <input type='text' placeholder='Product Color' value={color}  onChange = {event => setColor(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none w-[25rem]' required/>

    <input type='number' placeholder='Product Price' value={price}  onChange = {event => setPrice(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mt-5 mb-5 outline-none w-[25rem]' required/>

    <div className='flex items-center justify-center'>
     <label htmlFor='prodCategory' className='mr-5'>Category :</label>
   
     <select name='prodCategory' id='prodCategory' className='border w-[19rem]' onChange={event => setCategory(event.target.value)}>
        <option value='men'>Men</option>
        <option value='women'>Women</option>
        <option value='caps'>Cap</option>
     </select>
     </div>
     <div className='flex flex-col justify-center items-center space-y-2'>
     <button type='submit' className='p-3 bg-black text-white rounded-md mt-5 active:p-2'>UPDATE</button>
     <button type="button" className=" text-white bg-black hover:bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700"><Link as = {Link} to = '/admin'>BACK</Link></button>
     </div>
    </form>
    </div>

    </div>
  )
}

export default EditProd