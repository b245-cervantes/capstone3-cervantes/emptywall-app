import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { Button } from 'antd';
import {EditOutlined, DeleteOutlined} from '@ant-design/icons';
import { Link } from 'react-router-dom';
function ArchiveProd() {

 
    const [products, setProducts] = useState([]);


    useEffect(()=> {
        loadProduct();
    },[])

    const loadProduct = async () => {
        const result = await axios.get('http://localhost:4005/product/deleted', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        setProducts(result.data);
    }

  return (
    <div className='container mx-auto w-full 100v'>
    <h1 className='text-center text-2xl mt-10 mb-10'>ARCHIVED PRODUCT</h1>
    <div className="">

<div className="overflow-auto rounded-lg shadow block">
  <table className="w-full">
    <thead className="bg-gray-50 border-b-2 border-gray-200">
    <tr>
      <th className="w-20 p-3 text-sm font-semibold tracking-wide text-left">ID</th>
      <th className="p-3 text-sm font-semibold tracking-wide text-left">Name</th>
      <th className="w-24 p-3 text-sm font-semibold tracking-wide text-left">Description</th>
      <th className="w-24 p-3 text-sm font-semibold tracking-wide text-left">Img</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Size</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Color</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Price</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Category</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">isActive</th>
      <th className="w-32 p-3 text-sm font-semibold tracking-wide text-left">Operation</th>
    </tr>
    </thead>
    <tbody className="divide-y divide-gray-100">
        {products.map((product, index)=>(
            <tr key={product._id}>
               
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product._id}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.name}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.description}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.img}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.size}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.color}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.price}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.category}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">{product.isActive.toString()}</td>
                <td className="p-3 text-sm text-gray-700 whitespace-nowrap">
                <button><Link as = {Link} to ={`/admin/restore/${product._id}`}><EditOutlined  /></Link></button>
                </td>
            </tr>
        ))}
    </tbody>
  </table>
</div>
</div>
<div className='flex justify-center items-center mt-5'>
<button type="button" className=" text-white bg-black hover:bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700"><Link as = {Link} to = '/admin'>BACK</Link></button>
</div>
</div>
  )
}

export default ArchiveProd