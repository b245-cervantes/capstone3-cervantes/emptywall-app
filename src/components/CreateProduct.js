import React from 'react'


import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';


function CreateProduct() {
  
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [img, setImg] = useState("");
    const [size, setSize] =useState("")
    const [color, setColor] = useState("");
    const [price, setPrice] = useState("");
    const [category, setCategory] = useState("");

    const {user, setUser} = useContext(UserContext);
      const navigate = useNavigate();

      function addProduct (event) {
        event.preventDefault();

        fetch('http://localhost:4005/product/create', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`

            },
            body: JSON.stringify({
                name:name,
                description:description,
                img:img,
                size: size,
                color: color,
                price: price,
                category, category
            })
        })
        .then(result => result.json())
        .then(data => {
            if(user === false){

                Swal.fire({
                    title: 'ERRO ADD ITEM!!',
                    icon: 'error',
                    text: 'Please Try Again!'
                   
                })
            }else {
                
                Swal.fire({
                    title: `Then ${data.name} has been added successfully`,
                    icon: 'success',
                    text: 'Enjoy Shopping!',
                    
                   
                })
                console.log(data)
                console.log(user)
                navigate('/admin');

                setName('');
                setDescription('');
                setImg('');
                setSize('');
                setColor('');
                setPrice('');
                setCategory('');
            }
        })
      }

  return (
    user === false ?
    <Navigate to = '/*' />
        :
    <div className='container mx-auto 100v w-full'>
    <div className='flex flex-col justify-center items-center w-full'>
    <h2 className='text-2xl font-poppins mt-[100px] mb-5'>Add Product</h2>
    <form className='flex flex-col justify-center items-center' onSubmit={event => addProduct(event)}>
    <input type='text' placeholder='Product name' value={name}  onChange = {event => setName(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none w-[25rem]' required/>
      <textarea type='text' placeholder='Product Description' value={description}  onChange = {event => setDescription(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none w-[25rem]' required></textarea>
     <input type='text' placeholder='insert img link' value={img}  onChange = {event => setImg(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none w-[25rem]' required/>
    <div className='flex items-center justify-center'>
     <label htmlFor='size' className='mr-5'>Sizes :</label>
   
     <select name='size' id='size' className='border w-[21rem]' onChange={event => setSize(event.target.value)}>
        <option value=''>Choose sizes</option>
        <option value='allsizes'>All sizes</option>
        <option value='small'>Small</option>
        <option value='medium'>Medium</option>
        <option value='large'>Large</option>
        <option value='x-large'>Extra Large</option>
     </select>
     </div>

     <input type='text' placeholder='Product Color' value={color}  onChange = {event => setColor(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none w-[25rem]' required/>

    <input type='number' placeholder='Product Price' value={price}  onChange = {event => setPrice(event.target.value)}
     className='border border-gray-300 p-2 rounded-md font-poppins mt-5 mb-5 outline-none w-[25rem]' required/>

    <div className='flex items-center justify-center'>
     <label htmlFor='prodCategory' className='mr-5'>Category :</label>
   
     <select name='prodCategory' id='prodCategory' className='border w-[19rem]' onChange={event => setCategory(event.target.value)}>
        <option value=''>Choose Category</option>
        <option value='men'>Men</option>
        <option value='women'>Women</option>
        <option value='caps'>Cap</option>
     </select>
     </div>
     <div className='flex justify-center items-center flex-col space-y-2'>
     <button type='submit' className='p-3 bg-black text-white rounded-md mt-5 active:p-2'>CREATE PRODUCT</button>
     <button type="button" className=" text-white bg-black hover:bg-gray-800 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700"><Link as = {Link} to = '/admin'>BACK</Link></button>
     </div>
    </form>
    </div>

    </div>
  )
}

export default CreateProduct