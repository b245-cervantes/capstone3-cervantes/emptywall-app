import React from 'react'
import { Link } from 'react-router-dom'
import { BsArrowRight } from 'react-icons/bs'

function PageNotFound() {

    
  return (

    <div className='container mx-auto w-full 100v'>
    <div className='flex flex-col justify-center items-center'>
        
        <img src='https://i.ibb.co/yYf991d/notFound.png' style={{width: '780px'}} />

        <div className='text-center -mt-[110px] font-poppins'>
        <h1 className='text-[50px]'>There's NOTHING here...</h1>
        <h4 className='mb-5'>..maybe the page you're looking for is not found or never existed.</h4>

        <div className='mt-5 bg-black text-white w-[40%] mx-auto py-2 rounded-full flex justify-center items-center'>
            <button  className='w-full mr-[50px] active:p-1'> <Link as = {Link} to = '/'>Back to home</Link></button>
            <Link className='absolute mr-[-130px]'><BsArrowRight size={27}/></Link>
        </div>
        </div>

    </div>

    </div>
  )
}

export default PageNotFound