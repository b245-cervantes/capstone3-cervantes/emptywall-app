import React from 'react';
import { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { CartContext } from '../CartContext';
import { ProductContext } from '../ProductContext';


function ProductDetails() {
    const {_id} = useParams();
    const {products} = useContext(ProductContext);
    const {addToCart} = useContext(CartContext);
    const product = products.find((item) => {
        return item._id === _id;
    })
   
 
   
            
     
    if(!product) {
        return (
            <section className='h-screen flex justify-center'>
                Looading....

            </section>


        )
    }
    
    const {name, price, description, img} = product;
  return (
        <section className='pt-32 pb-12 lg:py-32 h-screen flex items-center'>
            <div className='container mx-auto'>
            <div className='flex flex-col lg:flex-row items-center'>
            <div className='flex flex-1 justify-center items-center mb-8 lg:mb-0'>
                <img className='max-w-[200px] lg:max-w-sm' src={img} />
            </div>
            <div className='flex-1 text-center lg:text-left'>
                <h1 className='text-[26px] font-medium mb-2 max-w-[450px] mx-auot'>{name}</h1>
                <div className='text-xl text-red-500 font-medium mb-6'>₱ {price}</div>
                <div>
                    <select className='border w-[21rem] mb-5'>
                        <option>Choose size:</option>
                        <option  value='small'>small</option>
                        <option  value='medium'>medium</option>
                        <option value='large'>large</option>
                        <option value='xlarge'>x-large</option>
                    </select>
                </div>
                <p className='mb-8'>{description}</p>
                <button onClick={() => addToCart(product, product._id)} className='bg-black py-4 px-8 text-white '>Add to cart</button>
            </div>
            </div>
                
            </div>
        </section>
  )
}

export default ProductDetails