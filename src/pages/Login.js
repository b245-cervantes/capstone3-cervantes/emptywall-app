
import emptLogo from '../assets/logo2.png';
import React from 'react';
import { useState, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';



function Login() {

        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        

        // const [user, setUser] = useState(localStorage.getItem('email'))
            const navigate = useNavigate();
        const {user,setUser} = useContext(UserContext);
        

     
         
        const login = (event) => {
            event.preventDefault()
            //if you want to add the email of the authenticatd user in local storage
            


            // Process a fecth request to corresponding backend API
            //syntax: fetch('url', {options})


                fetch(`http://localhost:4005/user/login`,{
                    method: 'POST',
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        email:email,
                        password:password
                    })
                }).then(result => result.json())
                .then(data => {

                    if(data ===false){
                        Swal.fire({
                            title: 'Authentication Failed!!!',
                            icon: 'error',
                            text: 'Please Try Again!'
                           
                        })
                    } else if(data.isAdmin === true){
                        localStorage.setItem('token', data.auth);
                        retrieveUserDetails(localStorage.getItem('token'))
                        Swal.fire({
                            title: `Welcome back !${data.firstName}`,
                            icon: 'success',
                            text: 'Enjoy Shopping!',
                            
                           
                        })
                        navigate('/admin');
                    }else{
                        localStorage.setItem('token', data.auth);
                        retrieveUserDetails(localStorage.getItem('token'))
                        Swal.fire({
                            title: `Welcome back !${data.firstName}`,
                            icon: 'success',
                            text: 'Enjoy Shopping!',
                            
                           
                        })
                        navigate('/');
                    }
                })

            // localStorage.setItem('email', email);
            // setUser(localStorage.getItem('email'))
            // alert("You are now Logged in!")
            // setEmail('');
            // setPassword('');
            // navigate('/')
        }
        const retrieveUserDetails = (token) => {
            // the token sent as part of the request's hearder information
            
            fetch("http://localhost:4005/user/", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(data => {
                console.log(data)

               setUser({
                id: data._id,
                isAdmin: data.isAdmin
               })
            })
        }
  return (
        user ?
        <Navigate to = '/*' />
        :

    <div className='container mx-auto w-full 100v'>     
        <div className='flex flex-col justify-center items-center'>
        <div>
            <img src={emptLogo} style={{width: '200px'}} />
        </div>
        <h2 className='text-2xl font-poppins -mt-11 mb-5'>Login Account</h2>
        <form className='flex flex-col w-[20rem]' onSubmit={event => login(event)}>
            <input type='email' placeholder='Email address' value={email} 
                onChange={event => setEmail(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none'/>
            <input type='password' placeholder='Password' value={password} 
                onChange={event => setPassword(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins outline-none'/>
            <h6 className='text-xs text-center mt-5 text-gray-400'>Forgotten your password?</h6>
            <h5 className='text-center text-xs mt-5'> By logging in, you agree to Emptywall's <a className='underline underline-offset-2'>Privacy Policy</a> and <a className='underline underline-offset-2'>Terms of Use</a></h5>
            <button className='p-3 bg-black text-white rounded-md mt-5'>SIGN IN</button>
            <h5 className='text-center text-xs mt-5 text-gray-400'>Not a Member? <Link as = {Link} to = '/register' className='underline underline-offset-2 text-black'>Join Us</Link></h5>
        </form>
        </div>
    </div>
  )
}

export default Login