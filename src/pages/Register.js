import React from 'react'
import emptLogo from '../assets/logo2.png'

import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

function Register() {
        const [email, setEmail] = useState("");
        const [firstName, setFirstName] = useState("");
        const [lastName, setLastName] = useState("");
        const [birthday, setBirthday] = useState("");
        const [password, setPassword] = useState("");
        const [confirmPassword, setConfirmPassword] = useState("");
        const [mobileNum, setMobileNum] = useState("");
        const [gender, setGender] = useState("")


        const {user, setUser} = useContext(UserContext);
        const navigate = useNavigate();

        function register(event){
          event.preventDefault()
           
          fetch("http://localhost:4005/user/register", {
           method: 'POST',
           headers: {
             'Content-Type' : 'application/json'
           },
           body: JSON.stringify({
             email:email,
             firstName:firstName,
             lastName:lastName,
             password: password,
             confirmPassword:confirmPassword,
             mobileNo: mobileNum,
             gender: gender
           })
          })
          .then(result => result.json())
          .then(data => {
            if(data) {
             console.log(data)
            Swal.fire({
               title: 'CONGRATULATIONS!!',
               icon: 'success',
               text: 'Your registration has been successful!'
              
           })
           navigate('/login')
           console.log(data)
            } else {
             
             Swal.fire({
               title: 'Registration Failed!!!',
               icon: 'error',
               text: 'Email is already in used!!'
              
           })
 
            }
          })
        }


  return (
          user ? 
          <Navigate to = '/*'/>
          :
        
    <div className='container mx-auto w-full 100v'>     
        <div className='flex flex-col justify-center items-center'>
        <div>
            <img src={emptLogo} style={{width: '200px'}} />
        </div>
        <h2 className='text-2xl font-poppins -mt-11 mb-5'>Register Account</h2>
        <form className='flex flex-col w-[20rem]' onSubmit={event => register(event)}>
            <input type='email' placeholder='Email address' id='email' value={email}  onChange = {event => setEmail(event.target.value)}
             className='border border-gray-300 p-2 rounded-md font-poppins mb-5 outline-none' required/>
            <input type='password' placeholder='Password' id='password' value={password}  onChange = {event => setPassword(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins outline-none' required/>
            <input type='password' placeholder='Confirm Password' id='Confirmpassword' value={confirmPassword}  onChange = {event => setConfirmPassword(event.target.value)}
            className='border border-gray-300 p-2 mt-5 rounded-md font-poppins outline-none' required/>
            <input type='text' placeholder='First Name' id='firstName'  value={firstName}  onChange = {event => setFirstName(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none' required/>
            <input type='text' placeholder='Last Name' id='lastName' value={lastName}  onChange = {event => setLastName(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none' required/>
            <input type='date' placeholder='Date of Birth' id='birthday' value={birthday}  onChange = {event => setBirthday(event.target.value)}
            className='border text-gray-400 border-gray-300 p-2 rounded-md mt-5 font-poppins outline-none' required/>
            <input type='text' placeholder='Mobile Number' id='mobileNum' value={mobileNum}  onChange = {event => setMobileNum(event.target.value)}
            className='border border-gray-300 p-2 rounded-md font-poppins mt-5 outline-none' required/>
            <div className='radio-toolbar flex items-center justify-between space-x-[9px] mr-10 mt-5'>
                <input type='radio' id='male'  name='gender' className='-mr-2' value='male' onChange = {event => setGender(event.target.value)}
                required/>
                <label htmlFor='male'>Male</label>
                <input type='radio' id='female' value='female' name='gender'   onChange = {event => setGender(event.target.value)}/>
            
                <label htmlFor='female'>Female</label>
            </div>
            <h6 className='text-xs text-center text-gray-400 mt-2'>Get a Emptywall Member Reward every year on your Birthday.</h6>
            <h5 className='text-center text-xs mt-5'> By creating an account, you agree to Emptywall's <Link className='underline underline-offset-2'>Privacy Policy</Link> and <Link className='underline underline-offset-2'>Terms of Use</Link></h5>
            <button type='submit' className='p-3 bg-black text-white rounded-md mt-5'>JOIN US</button>
            <h5 className='text-center text-xs mt-5 text-gray-400'>Already a Member? <Link className='underline underline-offset-2 text-black'>Sign In</Link></h5>
        </form>
        </div>
    </div>
  )
}

export default Register